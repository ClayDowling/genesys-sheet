APPNAME=genesyssheet
DEPENDENCIES=$(PWD)/dependencies

.PHONY: depclean clean all test

all: test
	$(MAKE) -C src DEPENDENCIES=$(DEPENDENCIES)

test: dependencies
	$(MAKE) -C test DEPENDENCIES=$(DEPENDENCIES)

depclean: clean
	rm -rf dependencies

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test clean

dependencies:
	install -d $(DEPENDENCIES)/lib
	install -d $(DEPENDENCIES)/include
	$(MAKE) -C ulfius/lib/orcania/src static-install PREFIX=$(DEPENDENCIES)
	$(MAKE) -C ulfius/lib/yder/src static-install PREFIX=$(DEPENDENCIES) ADDITIONALFLAGS=-I$(DEPENDENCIES)/include ADDITIONALLIBS=-L$(DEPENDENCIES)/lib
	$(MAKE) -C ulfius/src static-install PREFIX=$(DEPENDENCIES)
