#include "list.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h>


struct _list_t {
  size_t capacity;
  void *member[];
};

list_t *list_create(size_t count) {
  size_t allocsize = offsetof(struct _list_t, member) + count * sizeof(void *);
  list_t *lst =
      malloc(offsetof(struct _list_t, member) + count * sizeof(void *));
  memset(lst, 0, allocsize);
  lst->capacity = count;
  return lst;
}

size_t list_size(list_t *lst) { return lst->capacity; }

void *list_get(list_t *lst, size_t position) { return lst->member[position]; }

void list_set(list_t *lst, size_t position, void *data) {
  lst->member[position] = data;
}

void list_destroy(list_t *lst) { free(lst); }