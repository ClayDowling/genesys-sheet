#include "skill.h"
#include <stdlib.h>
#include <string.h>

const char *attribute_name[6] = {"BR", "AG", "INT", "CUN", "WILL", "PR"};

attribute_t attribute_from_string(const char *str) {
  for (int i = 0; i < ATTR_MAX; ++i) {
    if (strcmp(str, attribute_name[i]) == 0) {
      return i;
    }
  }
  return ATTR_MAX;
}
const char *attribute_to_string(attribute_t attr) {
  if (attr < ATTR_MAX) {
    return attribute_name[attr];
  }
  return "";
}

skill_t *skill_create(const char *name, int level, const char *category,
                      attribute_t attribute) {
  skill_t *s = (skill_t *)calloc(1, sizeof(skill_t));

  s->name = strdup(name);
  s->level = level;
  s->category = strdup(category);
  s->attribute = attribute;

  return s;
}

void skill_destroy(skill_t *s) {
  if (s->name)
    free(s->name);
  if (s->category)
    free(s->category);
  s->name = NULL;
  s->category = NULL;
  s->level = 0;
  s->attribute = ATTR_MAX;
}

int skill_cmp(void *left, void *right) {
  skill_t *lhs = (skill_t *)left;
  skill_t *rhs = (skill_t *)right;
  int result;

  result = strcmp(lhs->category, rhs->category);
  if (result < 0) {
    return -1;
  } else if (result > 0) {
    return 1;
  }

  result = strcmp(lhs->name, rhs->name);
  if (result < 0) {
    return -1;
  }
  if (result > 0) {
    return 1;
  }

  return 0;
}

json_t *skill_to_json(skill_t *s) {
  return json_pack("{s:s, s:i, s:b, s:b, s:s, s:s}", "name", s->name, "level",
                   s->level, "setting", s->setting, "career", s->career,
                   "category", s->category, "attribute",
                   attribute_to_string(s->attribute));
}

skill_t *skill_from_json(json_t *j) {
  const char *name;
  int level;
  int setting;
  int career;
  const char *category;
  const char *attribute;

  json_unpack(j, "{s:s, s:i, s:b, s:b, s:s, s:s}", "name", &name, "level",
              &level, "setting", &setting, "career", &career, "category",
              &category, "attribute", &attribute);

  skill_t *s =
      skill_create(name, level, category, attribute_from_string(attribute));
  s->career = career;
  s->setting = setting;

  return s;
}

list_t *skill_list_from_json(json_t *json) { return NULL; }
