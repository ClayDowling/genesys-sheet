#include "character.h"
#include "sheet.h"
#include <ulfius.h>


int callback_get_character(const struct _u_request *request,
                           struct _u_response *response, void *user_data) {
  json_error_t jerror;
  //  json_t *sheet = json_load_file("../resources/basesheet.json", 0, &jerror);
  json_t *sheet = json_loads(RAW_SHEET, 0, &jerror);
  if (!sheet) {
    ulfius_set_string_body_response(response, 500, jerror.text);
    y_log_message(Y_LOG_LEVEL_ERROR, jerror.text);
  }
  ulfius_set_json_body_response(response, 200, sheet);
  return U_CALLBACK_CONTINUE;
}