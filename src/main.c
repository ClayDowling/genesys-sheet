#include <stdio.h>
#include <stdlib.h>
#include <ulfius.h>

#include "character.h"

#define PORT 80

int main(int argc, char **argv) {
  struct _u_instance instance;

  y_init_logs("Genesys Character Sheet Server", Y_LOG_MODE_CONSOLE,
              Y_LOG_LEVEL_ERROR, NULL, "Initializing server");

  y_log_message(Y_LOG_LEVEL_ERROR, "I totally started bro");

  if (ulfius_init_instance(&instance, PORT, NULL, NULL) != U_OK) {
    fprintf(stderr, "Error ulfius_init_instance, dieing\n");
    return EXIT_FAILURE;
  }

  ulfius_add_endpoint_by_val(&instance, "GET", "/character", NULL, 0,
                             &callback_get_character, NULL);

  if (ulfius_start_framework(&instance) == U_OK) {
    printf("Started server on port %d\n", instance.port);
    getchar();
  }

  ulfius_stop_framework(&instance);
  ulfius_clean_instance(&instance);

  y_close_logs();

  return EXIT_SUCCESS;
}