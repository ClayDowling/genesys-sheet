#include "sheet.h"
#include "quote.h"

const char *RAW_SHEET = QUOTE({
  "name" : "",
  "player" : "",
  "archetype" : "",
  "career" : "",
  "description" : {
    "gender" : "",
    "age" : 0,
    "height" : "",
    "build" : "",
    "hair" : "",
    "eyes" : "",
    "features" : ""
  },
  "attributes" :
      {"br" : 0, "ag" : 0, "int" : 0, "cun" : 0, "will" : 0, "pr" : 0},
  "soak" : 0,
  "wounds" : {"threshold" : 0, "current" : 0},
  "strain" : {"threshold" : 0, "current" : 0},
  "defense" : {"ranged" : 0, "melee" : 0},
  "xp" : {"used" : 0, "unused" : 0},
  "skills" : [
    {
      "name" : "Alchemy",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "int"
    },
    {
      "name" : "Astrocartography",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "atttribute" : "int"
    },
    {
      "name" : "Athletics",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "br"
    },
    {
      "name" : "Computers",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "int"
    },
    {
      "name" : "Cool",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "pr"
    },
    {
      "name" : "Coordination",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "ag"
    },
    {
      "name" : "Discipline",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "will"
    },
    {
      "name" : "Driving",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "ag"
    },
    {
      "name" : "Mechanics",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "int"
    },
    {
      "name" : "Medicine",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "int"
    },
    {
      "name" : "Operating",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "int"
    },
    {
      "name" : "Perception",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "cun"
    },
    {
      "name" : "Piloting",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "ag"
    },
    {
      "name" : "Resilience",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "br"
    },
    {
      "name" : "Riding",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "ag"
    },
    {
      "name" : "Skulduggery",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "cun"
    },
    {
      "name" : "Stealth",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "ag"
    },
    {
      "name" : "Streetwise",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "cun"
    },
    {
      "name" : "Survival",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "cun"
    },
    {
      "name" : "Vigilance",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "general",
      "attribute" : "will"
    },
    {
      "name" : "Arcana",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "magic",
      "attribute" : "int"
    },
    {
      "name" : "Divine",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "magic",
      "attribute" : "will"
    },
    {
      "name" : "Primal",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "magic",
      "attribute" : "cun"
    },
    {
      "name" : "Brawl",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "combat",
      "attribute" : "br"
    },
    {
      "name" : "Gunnery",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "combat",
      "attribute" : "ag"
    },
    {
      "name" : "Melee",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "combat",
      "attribute" : "br"
    },
    {
      "name" : "Melee-Heavy",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "combat",
      "attribute" : "br"
    },
    {
      "name" : "Melee-Light",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "combat",
      "attribute" : "br"
    },
    {
      "name" : "Ranged",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "combat",
      "attribute" : "ag"
    },
    {
      "name" : "Ranged-Heavy",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "combat",
      "attribute" : "ag"
    },
    {
      "name" : "Ranged-Light",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "combat",
      "attribute" : "ag"
    },
    {
      "name" : "Charm",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "social",
      "attribute" : "pr"
    },
    {
      "name" : "Coercion",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "social",
      "attribute" : "will"
    },
    {
      "name" : "Deception",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "social",
      "attribute" : "cun"
    },
    {
      "name" : "Leadership",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "social",
      "attribute" : "pr"
    },
    {
      "name" : "Negotiation",
      "level" : 0,
      "setting" : false,
      "career" : false,
      "category" : "social",
      "attribute" : "pr"
    }
  ],
  "strength" : "",
  "flaw" : "",
  "desire" : "",
  "fear" : "",
  "money" : 0,
  "weapons" : [ {
    "name" : "",
    "skill" : "",
    "damage" : 0,
    "crit" : 0,
    "range" : "",
    "special" : ""
  } ],
  "gear" : ["item1"],
  "notes" : "",
  "talents" : [ {"name" : "", "page" : "", "tier" : 0, "description" : ""} ]
});