#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include <ulfius.h>

int callback_get_character(const struct _u_request *request,
                           struct _u_response *response, void *user_data);

#endif