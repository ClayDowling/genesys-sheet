#ifndef _SHEET_H_
#define _SHEET_H_

#include "jansson.h"
#include "list.h"
#include <stdbool.h>

typedef enum {
  ATTR_BR,
  ATTR_AG,
  ATTR_INT,
  ATTR_CUN,
  ATTR_WILL,
  ATTR_PR,
  ATTR_MAX
} attribute_t;

attribute_t attribute_from_string(const char *);
const char *attribute_to_string(attribute_t);

typedef struct {
  char *name;
  int level;
  bool setting;
  bool career;
  char *category;
  attribute_t attribute;
} skill_t;

skill_t *skill_create(const char *name, int level, const char *category,
                      attribute_t attribute);

void skill_destroy(skill_t *skill);

int skill_cmp(void *, void *);

json_t *skill_to_json(skill_t *);
skill_t *skill_from_json(json_t *);

list_t *skill_list_from_json(json_t *);

#endif