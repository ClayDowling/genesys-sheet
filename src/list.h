#ifndef _LIST_H_
#define _LIST_H_

#include <stdlib.h>

struct _list_t;
typedef struct _list_t list_t;

list_t *list_create(size_t);
void list_destroy(list_t *);
size_t list_size(list_t *);
void *list_get(list_t *, size_t);
void list_set(list_t *, size_t, void *);

#endif