#ifndef _TESTSUITE_
#define _TESTSUITE_

#include <check.h>

Suite *suite_character(void);
Suite *suite_skill(void);
Suite *suite_list(void);

#endif