#include "list.h"
#include <check.h>

#define LIST_SIZE 3

list_t *lst = NULL;

void setup(void) { lst = list_create(LIST_SIZE); }

void teardown(void) { list_destroy(lst); }

START_TEST(listCreate_byDefault_returnsNotNull) {
  list_t *actual = list_create(LIST_SIZE);
  ck_assert_ptr_ne(NULL, actual);
  list_destroy(actual);
}
END_TEST

START_TEST(listSize_givenListOfSize3_returns3) {
  list_t *actual = list_create(LIST_SIZE);
  ck_assert_int_eq(LIST_SIZE, list_size(actual));
  list_destroy(actual);

  actual = list_create(7);
  ck_assert_int_eq(7, list_size(actual));
  list_destroy(actual);
}
END_TEST

START_TEST(listGet_onEmptyLocation_returnsNull) {
  void *actual = list_get(lst, 1);
  ck_assert_ptr_eq(NULL, actual);
}
END_TEST

START_TEST(listGet_onSetLocation_returnsSetData) {
  list_set(lst, 0, (void *)7);
  void *actual = list_get(lst, 0);
  ck_assert_ptr_eq((void *)7, actual);
}
END_TEST

TCase *tcase_list(void) {
  TCase *tc = tcase_create("list");

  tcase_add_checked_fixture(tc, setup, teardown);
  tcase_add_test(tc, listCreate_byDefault_returnsNotNull);
  tcase_add_test(tc, listSize_givenListOfSize3_returns3);
  tcase_add_test(tc, listGet_onEmptyLocation_returnsNull);
  tcase_add_test(tc, listGet_onSetLocation_returnsSetData);
  return tc;
}

Suite *suite_list(void) {
  Suite *s = suite_create("list");
  suite_add_tcase(s, tcase_list());
  return s;
}