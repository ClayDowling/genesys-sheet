#include "character.h"
#include <check.h>
#include <jansson.h>

START_TEST(getCharacter_ByDefault_returnsEmptyCharacter) {
  struct _u_request request;
  struct _u_response response;
  json_t *expected = NULL;
  json_error_t jerror;

  ulfius_init_request(&request);
  ulfius_init_response(&response);

  callback_get_character(&request, &response, NULL);

  expected = json_load_file("../resources/basesheet.json", 0, &jerror);
  ck_assert(
      json_equal(expected, ulfius_get_json_body_response(&response, &jerror)));
}
END_TEST

START_TEST(getCharacter_byDefault_returnsCallbackContinue) {
  struct _u_request request;
  struct _u_response response;

  ulfius_init_request(&request);
  ulfius_init_response(&response);

  ck_assert_int_eq(U_CALLBACK_CONTINUE,
                   callback_get_character(&request, &response, NULL));
}
END_TEST

TCase *tcase_character(void) {
  TCase *tc = tcase_create("basic-character");
  tcase_add_test(tc, getCharacter_ByDefault_returnsEmptyCharacter);
  tcase_add_test(tc, getCharacter_byDefault_returnsCallbackContinue);
  return tc;
}

Suite *suite_character(void) {
  Suite *s = suite_create("character");

  suite_add_tcase(s, tcase_character());

  return s;
}