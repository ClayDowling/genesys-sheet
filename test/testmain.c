#include "testsuites.h"
#include <check.h>

int main(int argc, char **argv) {
  SRunner *runner;
  int number_fails;

  runner = srunner_create(suite_character());
  srunner_add_suite(runner, suite_skill());
  srunner_add_suite(runner, suite_list());
  srunner_run_all(runner, CK_NORMAL);
  number_fails = srunner_ntests_failed(runner);
  srunner_free(runner);

  return number_fails;
}