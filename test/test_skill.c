#include "quote.h"
#include "skill.h"
#include <check.h>

const char *SKILL_NAME = "stealin";
const int SKILL_LEVEL = 7;
const char *SKILL_CATEGORY = "mundane";
attribute_t SKILL_ATTRIBUTE = ATTR_AG;

START_TEST(skillcreate_byDefault_returnsPopulatedSkill) {
  skill_t *actual =
      skill_create(SKILL_NAME, SKILL_LEVEL, SKILL_CATEGORY, SKILL_ATTRIBUTE);
  ck_assert_str_eq(SKILL_NAME, actual->name);
  ck_assert_int_eq(SKILL_LEVEL, actual->level);
  ck_assert_str_eq(SKILL_CATEGORY, actual->category);
  ck_assert_int_eq(SKILL_ATTRIBUTE, actual->attribute);
  ck_assert_int_eq(false, actual->career);
  ck_assert_int_eq(false, actual->setting);

  skill_destroy(actual);
}
END_TEST

START_TEST(skillcmp_givenAandB_returnsNegativeOne) {
  skill_t *a = skill_create("A", 0, SKILL_CATEGORY, SKILL_ATTRIBUTE);
  skill_t *b = skill_create("B", 0, SKILL_CATEGORY, SKILL_ATTRIBUTE);

  ck_assert_int_eq(-1, skill_cmp((void *)a, (void *)b));

  skill_destroy(a);
  skill_destroy(b);
}
END_TEST

START_TEST(skillcmp_givenBandA_returnsOne) {
  skill_t *a = skill_create("A", 0, SKILL_CATEGORY, SKILL_ATTRIBUTE);
  skill_t *b = skill_create("B", 0, SKILL_CATEGORY, SKILL_ATTRIBUTE);

  ck_assert_int_eq(1, skill_cmp((void *)b, (void *)a));

  skill_destroy(a);
  skill_destroy(b);
}
END_TEST

START_TEST(skillcmp_givenAandA_returnsZero) {
  skill_t *a = skill_create("A", 0, SKILL_CATEGORY, SKILL_ATTRIBUTE);
  skill_t *b = skill_create("A", 0, SKILL_CATEGORY, SKILL_ATTRIBUTE);

  ck_assert_int_eq(0, skill_cmp((void *)b, (void *)a));

  skill_destroy(a);
  skill_destroy(b);
}
END_TEST

START_TEST(skillcmp_givenCatAandCatZ_returnsNegativeOne) {
  skill_t *a = skill_create(SKILL_NAME, 0, "A", SKILL_ATTRIBUTE);
  skill_t *b = skill_create(SKILL_NAME, 0, "Z", SKILL_ATTRIBUTE);

  ck_assert_int_eq(-1, skill_cmp(a, b));
}
END_TEST

START_TEST(skillcmp_givenCatZandCatA_returnsOne) {
  skill_t *a = skill_create(SKILL_NAME, 0, "Z", SKILL_ATTRIBUTE);
  skill_t *b = skill_create(SKILL_NAME, 0, "A", SKILL_ATTRIBUTE);

  ck_assert_int_eq(1, skill_cmp(a, b));
}
END_TEST

START_TEST(skillcmp_givenCatZandCatANameDandE_returnsOne) {
  skill_t *a = skill_create("D", 0, "Z", SKILL_ATTRIBUTE);
  skill_t *b = skill_create("E", 0, "A", SKILL_ATTRIBUTE);

  ck_assert_int_eq(1, skill_cmp(a, b));
}
END_TEST

START_TEST(skillToJson_givenSkill_returnsJsonSerialization) {
  const char *name;
  int level;
  int setting;
  int career;
  const char *category;
  const char *attribute;

  skill_t *src =
      skill_create(SKILL_NAME, SKILL_LEVEL, SKILL_CATEGORY, SKILL_ATTRIBUTE);
  src->setting = true;
  src->career = true;
  json_t *json = skill_to_json(src);

  ck_assert_ptr_ne(NULL, json);
  json_unpack(json, "{s:s, s:i, s:b, s:b, s:s, s:s}", "name", &name, "level",
              &level, "setting", &setting, "career", &career, "category",
              &category, "attribute", &attribute);

  ck_assert_str_eq(SKILL_NAME, name);
  ck_assert_int_eq(SKILL_LEVEL, level);
  ck_assert_int_eq(true, setting);
  ck_assert_int_eq(true, career);
  ck_assert_str_eq(SKILL_CATEGORY, category);
  ck_assert_str_eq("AG", attribute);
}
END_TEST

START_TEST(skillFromJson_givenJson_returnsDeserializedSkill) {
  skill_t *srcskill =
      skill_create(SKILL_NAME, SKILL_LEVEL, SKILL_CATEGORY, SKILL_ATTRIBUTE);
  json_t *json = skill_to_json(srcskill);
  skill_t *actual = skill_from_json(json);

  ck_assert_ptr_ne(NULL, actual);

  ck_assert_str_eq(srcskill->name, actual->name);
  ck_assert_int_eq(srcskill->level, actual->level);
  ck_assert_int_eq(srcskill->setting, actual->setting);
  ck_assert_int_eq(srcskill->career, actual->career);
  ck_assert_str_eq(srcskill->category, actual->category);
  ck_assert_int_eq(srcskill->attribute, actual->attribute);

  skill_destroy(srcskill);
  skill_destroy(actual);
}
END_TEST

START_TEST(skillListFromJson_givenJson_returnsSkillList) {
  skill_t *skill1 =
      skill_create(SKILL_NAME, 0, SKILL_CATEGORY, SKILL_ATTRIBUTE);
  skill_t *skill2 =
      skill_create("Second Skill", 0, SKILL_CATEGORY, SKILL_ATTRIBUTE);
  json_t *json1 = skill_to_json(skill1);
  json_t *json2 = skill_to_json(skill2);
  json_t *jsonlist = json_array();
  json_array_append(jsonlist, json1);
  json_array_append(jsonlist, json2);

  // list_t skills = skill_list_from_json(jsonlist);

  // skill_t *actual1 = skills.TOP;
}
END_TEST

// attributes

struct attr_to_str {
  attribute_t attr;
  const char *str;
};

struct attr_to_str atos[] = {
    {.attr = ATTR_BR, .str = "BR"},   {.attr = ATTR_AG, .str = "AG"},
    {.attr = ATTR_CUN, .str = "CUN"}, {.attr = ATTR_INT, .str = "INT"},
    {.attr = ATTR_PR, .str = "PR"},   {.attr = ATTR_WILL, .str = "WILL"},
    {.attr = ATTR_MAX, .str = NULL}};

START_TEST(attributeToString_convertsAttributeToStringRepresentation) {
  ck_assert_str_eq(atos[_i].str, attribute_to_string(atos[_i].attr));
}
END_TEST

START_TEST(attributeFromString_convertsStringToAttribute) {
  ck_assert_int_eq(atos[_i].attr, attribute_from_string(atos[_i].str));
}
END_TEST

TCase *tcase_skill() {
  TCase *tc = tcase_create("skills");
  tcase_add_test(tc, skillcreate_byDefault_returnsPopulatedSkill);
  tcase_add_test(tc, skillcmp_givenAandB_returnsNegativeOne);
  tcase_add_test(tc, skillcmp_givenBandA_returnsOne);
  tcase_add_test(tc, skillcmp_givenAandA_returnsZero);
  tcase_add_test(tc, skillcmp_givenCatAandCatZ_returnsNegativeOne);
  tcase_add_test(tc, skillcmp_givenCatZandCatA_returnsOne);
  tcase_add_test(tc, skillcmp_givenCatZandCatANameDandE_returnsOne);
  tcase_add_test(tc, skillToJson_givenSkill_returnsJsonSerialization);
  tcase_add_test(tc, skillFromJson_givenJson_returnsDeserializedSkill);
  tcase_add_test(tc, skillListFromJson_givenJson_returnsSkillList);
  return tc;
}

TCase *tcase_attribute() {
  TCase *tc = tcase_create("attributes");
  tcase_add_loop_test(
      tc, attributeToString_convertsAttributeToStringRepresentation, 0, 5);
  tcase_add_loop_test(tc, attributeFromString_convertsStringToAttribute, 0, 5);
  return tc;
}

Suite *suite_skill() {
  Suite *s = suite_create("skill");
  suite_add_tcase(s, tcase_attribute());
  suite_add_tcase(s, tcase_skill());
  return s;
}