# A REST Server Written In C

If you want to write a modern REST service, no sane person will suggest that you do it in C.  That's why I'm going to do it in C, and I'm going to take you, dear reader, along for the ride.

It's not actually as mad as it might sound on the face of it.  I recently found [Ulfius](https://babelouest.github.io/ulfius/), a library written on top of [libmicrohttpd](https://www.gnu.org/software/libmicrohttpd/) which claims to make writing REST servers easy.  Having suffered through writing several enterprise java web services with Spring, I'm game for something simple and fast.

## The Application

I'm a big fan of table top role playing games, and I've become enamored of Fantasy Flight's new Genesys system.  So I'm going to make a web service which can track a character in the system.

## The Setup

For professional reasons I've taken to working on a windows machine (i.e. my current professional work is on a C# project), I'm going to need a docker container, because Ulfius, like most C libraries, is designed around the idea that I'm going to work on a Unix machine.  Also, if you've compared hosting prices for Windows and Linux servers, you'll be writing for Linux as well.

I'll base this on Ubuntu 18.04.  As I am writing this, it is February of 2018, so it hasn't been officially released yet, but the docker images are out there now.  Most importantly, Ubuntu 18.04 has a recent version of Ulfius and all of its necessary libraries, so it will be an easy platform to work from.

To get Ulfius and all of its dependencies, I'll install `libulfius-dev`.  I wnat to do Test Driven Development, so I'm also going to include my favorite C test suite, [check](https://libcheck.github.io/check/).  There's nothing magical about using it for this project, it's just a personal preference, so use whatever test suit strikes your fancy.

The base Ubuntu image ships with almost nothing, so I need to add common development tools.  The packages I want are:

* `vim` - the editor of choice for the grey bearded Unix programmer.
* `build-essential` - a lovely collection of the latest development tools, most importantly including a C compiler and related tools.
* `git` - for version control.  I'm hosting this project on [GitLab](https://gitlab.com/ClayDowling/genesys-sheet).

## Utilities

I've got a brain like a steel trap for docker commands - all rusty and broken.  So to save constant Googling of the commands, I've made scripts for the commands I use most.  I've also added a target to the Makefile explicitly to build the docker container with the newest version of the Dockerfile.

The batch files in the root directory encapsulate all of the common commands that I'm likely to use:

* Launch a new container with the name genesyssheetdev and the source folder for the project mapped to /src in the container.
* Start an existing genesyssheetdev container.
* Enter a runnning container.
* Destroy a container so that it can be rebuilt.

## Conclusion

That's probably enough for our first session: getting the container built with the right components, and building up supporting scripts around it.  In the next session we'll try implementing a simple GET method in the web service.