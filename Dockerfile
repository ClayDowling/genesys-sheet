FROM ubuntu:18.04

EXPOSE 80

RUN apt update && apt install -y vim libulfius-dev build-essential git \
    check pkg-config man uuid-dev libmicrohttpd-dev libcurl4-gnutls-dev
